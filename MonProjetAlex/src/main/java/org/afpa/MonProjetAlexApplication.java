package org.afpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonProjetAlexApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonProjetAlexApplication.class, args);
	}

}
